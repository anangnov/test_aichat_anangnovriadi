"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    "voucher",
    {
      code: {
        type: DataTypes.STRING,
        allowNull: false
      },
      customer_id: {
        type: DataTypes.INTEGER,
        allowNull: true
      },
      status: {
        type: DataTypes.STRING,
        allowNull: true
      },
      lock: {
        type: DataTypes.STRING,
        allowNull: true
      },
      lock_at: {
        type: DataTypes.STRING,
        allowNull: true
      }
    },
    {
      tableName: "vouchers",
      timestamps: false
    }
  );
};
