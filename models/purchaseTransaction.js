"use strict";

module.exports = function(sequelize, DataTypes) {
  return sequelize.define(
    "purchaseTransaction",
    {
      total_spent: {
        type: DataTypes.STRING,
        allowNull: true
      },
      total_saving: {
        type: DataTypes.STRING,
        allowNull: true
      },
      customer_id: {
        type: DataTypes.INTEGER,
        allowNull: true
      }
    },
    {
      tableName: "purchase_transaction",
      timestamps: false
    }
  );
};
