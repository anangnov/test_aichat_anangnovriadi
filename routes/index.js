const express = require("express");
const router = express.Router();
const customerController = require("../controller/customerController");
const transactionController = require("../controller/transactionController");
const submissionController = require("../controller/submissionController");

module.exports = app => {
  router.get("/customer", customerController.listCustomer);
  router.post("/transaction", transactionController.createTransaction);
  router.get("/voucher/generate", transactionController.generateVoucher);
  router.post("/submission/check", submissionController.checkSubmission);
  router.post(
    "/submission/check-photo",
    submissionController.checkSubmissionPhoto
  );

  app.use("/api", router);
};
