-- -------------------------------------------------------------
-- TablePlus 3.12.8(368)
--
-- https://tableplus.com/
--
-- Database: test_aichat
-- Generation Time: 2021-06-02 01:18:39.2900
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `purchase_transaction`;
CREATE TABLE `purchase_transaction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `total_spent` decimal(10,2) DEFAULT NULL,
  `total_saving` decimal(10,2) DEFAULT NULL,
  `transaction_at` date DEFAULT NULL,
  `customer_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `purchase_transaction_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `status` tinyint DEFAULT '1',
  `lock_at` timestamp NULL DEFAULT NULL,
  `customer_id` int DEFAULT NULL,
  `lock` tinyint DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `gender`, `date_of_birth`, `contact_number`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Anang', 'Novriadi', 'L', '1996-11-02', '085816203961', 'anangnov99@gmail.com', '2021-06-01 20:15:50', '2021-06-01 20:15:59'),
(2, 'Alex', 'Jaynudin', 'L', '1994-11-05', '085816204962', 'alex@gmail.com', '2021-06-01 20:15:50', '2021-06-01 20:15:59'),
(5, 'Indah', 'Sari', 'P', '1993-02-05', '08141204962', 'indah@gmail.com', '2021-06-01 20:16:41', '2021-06-01 20:16:41');

INSERT INTO `purchase_transaction` (`id`, `total_spent`, `total_saving`, `transaction_at`, `customer_id`) VALUES
(1, 12.00, 2.00, '2021-05-04', 1),
(2, 28.00, 4.00, '2021-05-04', 1),
(3, 58.00, 4.00, '2021-06-01', 1),
(4, 28.10, 4.00, '2021-06-01', 1);

INSERT INTO `vouchers` (`id`, `code`, `status`, `lock_at`, `customer_id`, `lock`) VALUES
(1, 'dPN9J15i', 2, '2021-06-02 07:29:59', 1, 1),
(2, 'dbXWLWDY', 1, NULL, NULL, 0),
(3, 'Ipy1D5jN', 1, NULL, NULL, 0),
(4, 'HGTkF73P', 1, NULL, NULL, 0),
(5, '0TW2dynk', 1, NULL, NULL, 0),
(6, 'TLe7KeIu', 1, NULL, NULL, 0),
(7, 'p9wD5fN0', 1, NULL, NULL, 0),
(8, 'oxVs3vbx', 1, NULL, NULL, 0),
(9, 'R1bFOZIS', 1, NULL, NULL, 0),
(10, 'A2rkmmRr', 1, NULL, NULL, 0),
(11, 'TPCGf1Y9', 1, NULL, NULL, 0),
(12, 'YBA59vGM', 1, NULL, NULL, 0),
(13, 'VxUVVCZm', 1, NULL, NULL, 0),
(14, 'TRmUlU7N', 1, NULL, NULL, 0),
(15, '1g9PiG8Y', 1, NULL, NULL, 0),
(16, 'CQMpeL7I', 1, NULL, NULL, 0),
(17, 'xZul2Bav', 1, NULL, NULL, 0),
(18, '91T6uulE', 1, NULL, NULL, 0),
(19, 'rCvTD4ZO', 1, NULL, NULL, 0),
(20, 'xpawS13F', 1, NULL, NULL, 0),
(21, 'bLyOKQFv', 1, NULL, NULL, 0),
(22, 'hpLKIzuY', 1, NULL, NULL, 0),
(23, 'a6SuZKmv', 1, NULL, NULL, 0),
(24, 'N9DInxGq', 1, NULL, NULL, 0),
(25, '5rx3opAT', 1, NULL, NULL, 0),
(26, '8MGYYfYL', 1, NULL, NULL, 0),
(27, '9xJR1D81', 1, NULL, NULL, 0),
(28, 'S3tgBuSu', 1, NULL, NULL, 0),
(29, 'UCFYQMng', 1, NULL, NULL, 0),
(30, '6F9qY5Kt', 1, NULL, NULL, 0),
(31, 'd4icrhQ1', 1, NULL, NULL, 0),
(32, '0cA7sutK', 1, NULL, NULL, 0),
(33, 's7uRv1P0', 1, NULL, NULL, 0),
(34, 'ySaS1AbZ', 1, NULL, NULL, 0),
(35, 'ahdGpKbB', 1, NULL, NULL, 0),
(36, 'z2WbvgVG', 1, NULL, NULL, 0),
(37, 'J9sFnZwo', 1, NULL, NULL, 0),
(38, 'QXzUmDzg', 1, NULL, NULL, 0),
(39, 'ydN6vuoT', 1, NULL, NULL, 0),
(40, 'X484B3F1', 1, NULL, NULL, 0),
(41, '4tymg9Fh', 1, NULL, NULL, 0),
(42, 'RkerAFxx', 1, NULL, NULL, 0),
(43, 'K0hdz7Iw', 1, NULL, NULL, 0),
(44, 'pWo3AWuW', 1, NULL, NULL, 0),
(45, 'uCkklnPc', 1, NULL, NULL, 0),
(46, 'ib33u9OG', 1, NULL, NULL, 0),
(47, '7yMkTZFW', 1, NULL, NULL, 0),
(48, 'mMSETfBH', 1, NULL, NULL, 0),
(49, 'MLjRZh88', 1, NULL, NULL, 0),
(50, 't2EYgERU', 1, NULL, NULL, 0),
(51, 'xvcLhK36', 1, NULL, NULL, 0),
(52, 'nZCzMd2M', 1, NULL, NULL, 0),
(53, 'Bn7HSthS', 1, NULL, NULL, 0),
(54, 'lbTHx064', 1, NULL, NULL, 0),
(55, 'hgRTQGTB', 1, NULL, NULL, 0),
(56, '6rTx592P', 1, NULL, NULL, 0),
(57, 'FPCte00w', 1, NULL, NULL, 0),
(58, '0atuqEwF', 1, NULL, NULL, 0),
(59, 'M96I2Zfd', 1, NULL, NULL, 0),
(60, 'eFFBECGn', 1, NULL, NULL, 0),
(61, 'WaxUvbXc', 1, NULL, NULL, 0),
(62, 'kxyJ9JvP', 1, NULL, NULL, 0),
(63, 'f78YuY6w', 1, NULL, NULL, 0),
(64, '3W66NonK', 1, NULL, NULL, 0),
(65, 'bfy7EGt3', 1, NULL, NULL, 0),
(66, 'i2GuZSrc', 1, NULL, NULL, 0),
(67, 'GiPa63Yi', 1, NULL, NULL, 0),
(68, 'y4eIdeVl', 1, NULL, NULL, 0),
(69, 'Lnhs6Wfu', 1, NULL, NULL, 0),
(70, 'upsTY0VT', 1, NULL, NULL, 0),
(71, 'HUhEw0Jl', 1, NULL, NULL, 0),
(72, '2zz4xjUv', 1, NULL, NULL, 0),
(73, 'JmyDqHwY', 1, NULL, NULL, 0),
(74, 'eVnAQCm5', 1, NULL, NULL, 0),
(75, 'F53ZQSMG', 1, NULL, NULL, 0),
(76, 'dICc9seX', 1, NULL, NULL, 0),
(77, 'u42H6ShY', 1, NULL, NULL, 0),
(78, 'YM2BLgxc', 1, NULL, NULL, 0),
(79, 'Qyuioxcd', 1, NULL, NULL, 0),
(80, '6GxwnRGH', 1, NULL, NULL, 0),
(81, '5sJdxlF8', 1, NULL, NULL, 0),
(82, 'bXDnayAO', 1, NULL, NULL, 0),
(83, '983EghOA', 1, NULL, NULL, 0),
(84, 'eguiSsXv', 1, NULL, NULL, 0),
(85, 'afW76QHl', 1, NULL, NULL, 0),
(86, 'OIoCi5g7', 1, NULL, NULL, 0),
(87, 'Y3YFVPQZ', 1, NULL, NULL, 0),
(88, 'W0i3eLGf', 1, NULL, NULL, 0),
(89, '0p8dz6Gg', 1, NULL, NULL, 0),
(90, 'FERPdYpd', 1, NULL, NULL, 0),
(91, 'D514ppN1', 1, NULL, NULL, 0),
(92, 'oQ2As5qr', 1, NULL, NULL, 0),
(93, 'utA1psjo', 1, NULL, NULL, 0),
(94, 'HekbRSt7', 1, NULL, NULL, 0),
(95, 'GJqtj4Fi', 1, NULL, NULL, 0),
(96, '5QmJ1NsV', 1, NULL, NULL, 0),
(97, 'rSv4zph9', 1, NULL, NULL, 0),
(98, 'fhPzxzyQ', 1, NULL, NULL, 0),
(99, '8oIK27o0', 1, NULL, NULL, 0),
(100, '7GZtmTgE', 1, NULL, NULL, 0);



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;