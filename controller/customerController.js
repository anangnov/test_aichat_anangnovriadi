"use strict";

const models = require("../models");

// List Customer
const listCustomer = async (req, res) => {
  let customer = await models.customer.findAll({});

  return req.output(
    req,
    res,
    {
      error: false,
      message: "Found",
      data: customer,
      count: customer.length
    },
    "info",
    200
  );
};

module.exports = { listCustomer };
