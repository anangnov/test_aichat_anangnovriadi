"use strict";

const async = require("async");
const models = require("../models");
const moment = require("moment");
const { Op, Sequelize } = require("sequelize");

// Submission process
const checkSubmission = (req, res) => {
  let query = {};

  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "customer_id",
            value: req.body.customer_id
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkCustomer(data, callback) {
        models.voucher
          .findAll({
            where: { customer_id: req.body.customer_id }
          })
          .then((result, err) => {
            if (result.length < 1) {
              callback(null, data);
            } else {
              callback({
                error: false,
                message:
                  "You have process the voucher before, please complete the submission photo",
                status: 200
              });
            }
          });
      },
      function checkTotalSpent(data, callback) {
        let now = moment().format("YYYY-MM-DD");
        let lastMonth = moment(now, "YYYY-MM-DD").subtract(30, "days");
        lastMonth = moment(lastMonth).format("YYYY-MM-DD");
        query.where = {
          transaction_at: {
            [Op.between]: [lastMonth, now]
          }
        };

        models.purchaseTransaction.findAll(query).then((result, err) => {
          if (err) return callback(err);
          let total = 0;

          for (let i = 0; i < result.length; i++) {
            total += Number(result[i].total_spent);
          }

          let lockAt = moment().format("YYYY-MM-DD HH:mm:ss");
          let json = {
            lockAt: lockAt,
            total: total
          };

          // check total spent
          if (total >= 100) {
            callback(null, json);
          } else {
            return callback({
              error: false,
              message: "Not Eligible",
              status: 200
            });
          }
        });
      },
      function getVoucher(data, callback) {
        query.where = {
          status: 1,
          lock: 0
        };

        models.voucher.findAll(query).then((result, err) => {
          let voucherId = result[0].id;

          models.voucher
            .update(
              {
                customer_id: req.body.customer_id,
                lock_at: data.lockAt,
                lock: 1
              },
              {
                where: {
                  id: voucherId
                }
              }
            )
            .then(() => {
              callback(null, {
                error: false,
                message: "Updated and Eligible",
                info: "Please submit your photo and get the voucher"
              });
            });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", err.status || 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

// Submission photo
const checkSubmissionPhoto = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "customer_id",
            value: req.body.customer_id
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getVoucher(data, callback) {
        models.voucher
          .findOne({
            where: {
              customer_id: req.body.customer_id
            }
          })
          .then((result, err) => {
            if (err) return callback(err);
            if (result == null) {
              return callback({
                error: false,
                message: "Not Found",
                status: 200
              });
            } else {
              callback(null, result);
            }
          });
      },
      function checkPhotoAndSubmission(data, callback) {
        let photo = true;
        let a = moment(new Date(), "YYYY-MM-DD HH:mm:ss");
        let b = moment(data.lock_at, "YYYY-MM-DD HH:mm:ss");
        let d = moment.duration(a.diff(b));

        if (photo && Number(Math.abs(d.asMinutes())) < 10) {
          models.voucher
            .update(
              {
                lock: 0,
                lock_at: null,
                customer_id: null
              },
              {
                where: {
                  customer_id: req.body.customer_id
                }
              }
            )
            .then((result, err) => {
              callback(null, {
                error: false,
                message: "Not Process",
                info: "You does not meet the qualifications"
              });
            });
        } else {
          models.voucher
            .update(
              {
                status: 2
              },
              {
                where: { customer_id: req.body.customer_id }
              }
            )
            .then((result, err) => {
              callback(null, {
                error: false,
                message: "Process",
                info: "You meet the qualifications",
                data: {
                  code_voucher: data.code,
                  customer_id: data.customer_id
                }
              });
            });
        }
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", err.status || 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { checkSubmission, checkSubmissionPhoto };
