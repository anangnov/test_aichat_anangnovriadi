"use strict";

const models = require("../models");
const randomize = require("randomatic");

// Create transaction
const createTransaction = async (req, res) => {
  let transaction = await models.purchaseTransaction.create({
    customer_id: req.body.customer_id,
    total_spent: req.body.total_spent,
    total_saving: req.body.total_saving
  });

  return req.output(
    req,
    res,
    {
      error: false,
      message: "Success",
      data: transaction
    },
    "info",
    201
  );
};

// Test data
// Generate code voucher dummy
const generateVoucher = async (req, res) => {
  for (let i = 0; i < 100; i++) {
    let rand = randomize("Aa0", 8);
    await models.voucher.create({
      code: rand
    });
  }
};

module.exports = { createTransaction, generateVoucher };
